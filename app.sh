sudo apt-get install espeak mbrola mbrola-en1 alsa-utils
filename=$1
echo $1
while read line
do
echo $line
if [[ $line == *";volup"* ]]; then
volup_percentage=$(echo $line | awk '{print $2}')
amixer set 'Master' $volup_percentage%+
elif [[ $line == *";voldown"* ]]; then
voldown_percentage=$(echo $line | awk '{print $2}')
amixer set 'Master' $voldown_percentage%-
elif [[ $line == *";mute"* ]]; then
 amixer set Master mute
elif [[ $line == *";unmute"* ]]; then
 amixer set Master unmute
 amixer set Front unmute
 amixer set Headphone unmute
 amixer set Surround unmute
 amixer set Center unmute
  amixer set LFE unmute
else
espeak -v mb-en1 -s 120 "$line"
fi
done < $1

